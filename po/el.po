# Greek translation for libsecret.
# Copyright (C) 2013 libsecret's COPYRIGHT HOLDER
# This file is distributed under the same license as the libsecret package.
# Dimitris Spingos <dmtrs32@gmail.com>, 2013.
# Dimitris Spingos (Δημήτρης Σπίγγος) <dmtrs32@gmail.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: libsecret master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=libsecret&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-12-19 15:14+0000\n"
"PO-Revision-Date: 2014-07-09 14:50+0200\n"
"Last-Translator: Tom Tryfonidis <tomtryf@gmail.com>\n"
"Language-Team: team@lists.gnome.gr\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.5\n"
"X-Project-Style: gnome\n"

#: ../libsecret/secret-item.c:1161
#, c-format
msgid "Received invalid secret from the secret storage"
msgstr "Λήψη μη έγκυρου μυστικού από τη μυστική τοποθεσία αποθήκευσης"

#: ../libsecret/secret-methods.c:1055
msgid "Default keyring"
msgstr "Προεπιλεγμένη κλειδοθήκη"

#: ../libsecret/secret-session.c:244 ../libsecret/secret-session.c:281
msgid "Couldn’t communicate with the secret storage"
msgstr "Αδύνατη η επικοινωνία με τη μυστική τοποθεσία αποθήκευσης"

#: ../tool/secret-tool.c:39
msgid "the label for the new stored item"
msgstr "η ετικέτα για το νεοαποθηκευμένο στοιχείο"

#: ../tool/secret-tool.c:41
msgid "the collection in which to place the stored item"
msgstr "η συλλογή στην οποία θα τοποθετηθεί το αποθηκευμένο στοιχείο"

#: ../tool/secret-tool.c:43 ../tool/secret-tool.c:50 ../tool/secret-tool.c:437
msgid "attribute value pairs of item to lookup"
msgstr "ιδιότητα ζευγών τιμών του στοιχείου προς αναζήτηση"

#: ../tool/secret-tool.c:57
msgid "attribute value pairs which match items to clear"
msgstr "ιδιότητα ζευγών τιμών που ταιριάζει με τα στοιχεία προς εκκαθάριση"

#: ../tool/secret-tool.c:433
msgid "return all results, instead of just first one"
msgstr "επιστροφή όλων των αποτελεσμάτων, αντί να επιστρέφεται μόνο το πρώτο"

#: ../tool/secret-tool.c:435
msgid "unlock item results if necessary"
msgstr "ξεκλείδωμα των αποτελεσμάτων στοιχείου, αν απαιτείται"
